import os

from uvicorn import run
from fastapi import FastAPI

app = FastAPI()

DATA_PATH = os.path.join(os.path.dirname(__file__), "data.txt")


@app.get("/readinglist")
async def get_reading_list():
    """provide the entire reading list in first in, in order added"""

    # Read plain text file, split lines into individual ids
    with open(DATA_PATH) as data:
        contents = data.read().splitlines()

    return {"id": contents}


# Note that with multiple users we would use the requets body for the
# data to be inserted, however, I am only using the id for the publication
# so will simply take the value from the path.
@app.put("/readinglist/{item_id}")
async def put_reading_list_item(item_id):
    """Insert publication ids to the users reading list"""

    with open(DATA_PATH, 'a') as data:

        data.write("\n") # add new line
        data.write(str(item_id))

    return {"item_id": item_id}



def start():
    """Launch with `poetry run start` at root level"""
    run("pubmed_backend.main:app", host="0.0.0.0", port=8000, reload=True)