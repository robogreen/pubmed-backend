# Pubmed Search App Service

## About

Very simple service to GET or POST unique IDs of publications. 
For expediency sake, I have chosen to implement a very simple
list of results stored in a plain text file adjacent to the
application. 

## Installing Dependencies

### `poetry install`

## Running the app

Start the app in development mode:
### `poetry run start`

## Run Tests
### `poetry run pytest`

## Improvements
- [] Test coverage
- [] Add pagination
- [] Data validation
- [] Auth
- [] Error handling
- [] Prevent duplicates