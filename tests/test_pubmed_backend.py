import os
import pytest

from pubmed_backend import __version__
from fastapi.testclient import TestClient
from pubmed_backend.main import app
from unittest import mock


client = TestClient(app)



TEST_DATA_PATH = os.path.join(os.path.dirname(__file__), "test_data.txt")


@pytest.fixture
def temp_test_file(tmp_path, test_data):
    """Create a temporay file for testing the app with seed data"""

    tmp_filepath = os.path.join(tmp_path, "temp_test_data.txt")

    with open(tmp_filepath, 'a') as tmp_file:
        tmp_file.write("\n".join(test_data))
    
    return os.path.abspath(tmp_filepath)
    

@pytest.fixture
def test_data():
    """Pytest fixture to provide tests with the test data"""
    with open(TEST_DATA_PATH) as test_data:
        data = test_data.read().splitlines()
        yield data
    

@pytest.fixture
def mock_app_data(temp_test_file):
    """Pytest fixture to patch the app data source with test data"""
    with mock.patch("pubmed_backend.main.DATA_PATH", temp_test_file):
        yield


def test_version():
    assert __version__ == '0.1.0'


def test_get_reading_list(test_data, mock_app_data):
    response = client.get("/readinglist")
    assert response.status_code == 200
    assert response.json() == {"result": test_data}


def test_put_reading_list_item(test_data, mock_app_data):
    test_id = 999999
    put_response = client.put("/readinglist/{}".format(test_id))
    assert put_response.status_code == 200
    assert put_response.json() == {'id': test_id}

    get_response = client.get("/readinglist")
    assert get_response.status_code == 200
    assert get_response.json() == {"result": test_data + test_id}

    
